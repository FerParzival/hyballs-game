﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Script : MonoBehaviour
{
    public GameObject panelWin;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1.0f;
        panelWin.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CargarJuego(string escena)
    {
        SceneManager.LoadScene(escena);
    }
    public void CargarMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void Win()
    {
        
        if (Time.timeScale == 0.0f)
        {
            Time.timeScale = 1.0f;
        }
        panelWin.SetActive(true);
    }
     public void Pause()
    {
        Time.timeScale = 0.0f;
    }
    public void UnPause()
    {
        Time.timeScale = 1.0f;
    }
    public void Quit()
    {
        Application.Quit();
    }
}
