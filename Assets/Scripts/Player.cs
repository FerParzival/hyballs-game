﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GooglePlayGames;

public class Player : MonoBehaviour
{
    public Material[] material;
    private Material actualMaterial;
    public float salto;
    public GameObject script;
    public Texture[] textura;

    int platformCounter;

    int hasDied;

    int winCounter;

    bool hasWin;

    void Start()
    {
        this.GetComponent<Rigidbody>().AddForce(Vector3.up * salto * 50, ForceMode.Impulse);
        platformCounter = 0;
        hasDied = 0;
        hasWin = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            this.GetComponent<Rigidbody>().AddForce(Vector3.up * salto, ForceMode.Impulse);
        }
        else
        {
            this.GetComponent<Rigidbody>().AddForce(Vector3.zero);
        }

        //material[0].SetTexture("Yellow", textura[0]);
        if (platformCounter >= 2)
        {
            //Bool achievement
            PlayGamesPlatform.Instance.ReportProgress(
                GPGSIds.achievement_buen_intento, 100.0f, (bool success) =>
                 {
                     Debug.Log("Dos plataformas" + success);
                 });
        }
        
    }

    public void AddScore(int score)
    {
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ReportScore(score, GPGSIds.leaderboard_plataformas_superadas, (bool success) =>
             {
                 Debug.Log("Score actualizado" + success);
             });
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Moneda")
        {
            actualMaterial = material[Random.Range(0, material.Length)];
            this.GetComponent<Renderer>().material = actualMaterial;
            
        }
        if (other.tag == "Meta")
        {
            //SceneManager.LoadScene("Menu");
            script.GetComponent<Script>().Win();
            hasWin = true;
            winCounter += 1;
            PlayGamesPlatform.Instance.Events.IncrementEvent(GPGSIds.event_has_ganado, 1);
            Time.timeScale = 0.0f;
        }
        if (other.tag == "Muerte")
        {
            this.transform.position = new Vector3(0.0f, -5.5f, 0.0f);
            this.GetComponent<Rigidbody>().AddForce(Vector3.up * salto, ForceMode.Impulse);
            if (script.GetComponent<UI>().intScore >= 1)
            {
                script.GetComponent<UI>().intScore -= 10;
            }
        }
        if (other.tag == "Plataforma" && other.gameObject.GetComponent<Renderer>().material.name == actualMaterial.name + " (Instance)")
        {
            print("Pasele joven Bv");
            script.GetComponent<UI>().intScore += 10;

            platformCounter += 1;

            if (platformCounter >= 5 && hasDied <= 3)
            {
                AddScore(script.GetComponent<UI>().intScore);
            }
        }
        if(other.tag == "Plataforma" && other.gameObject.GetComponent<Renderer>().material.name != actualMaterial.name + " (Instance)")
        {
            print("Quiero Hay prro >:V");

            hasDied += 1;

            this.transform.position = new Vector3(0.0f,-5.5f,0.0f);
            this.GetComponent<Rigidbody>().AddForce(Vector3.up * salto, ForceMode.Impulse);
            if (script.GetComponent<UI>().intScore >= 1)
            {
                script.GetComponent<UI>().intScore -= 10;
            }
            //Debug.Log("OTHER: " + other.gameObject.GetComponent<Renderer>().material.name);
            //Debug.Log("THIS: " + actualMaterial.name);
            //SceneManager.LoadScene("SampleScene");
        }
    }
}
