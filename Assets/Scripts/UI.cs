﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;

public class UI : MonoBehaviour
{
    public Text score;
    public int intScore;

    // Update is called once per frame
    void Update()
    {
        score.text = "Score: " + intScore.ToString();
    }

    public void ShowAchievementsUI()
    {
        Social.ShowAchievementsUI();
    }

    public void ShowLeaderboard()
    {
        Social.ShowLeaderboardUI();
    }

}
