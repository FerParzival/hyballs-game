﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public class Plataforma : MonoBehaviour
{
    public Transform[] points;
    public GameObject plataforma;
    int destPoint;
    public float allowence = 5f;
    public float speed;

    public GameObject[] cubos;
    public Material[] colores;
    public int[] numColor;
    // Use this for initialization
    void Start()
    {
        // Set first target
        UpdateTarget();
        //Colores
        for (int i = 0; i < cubos.Length;i++)
        {
            int colornumer = Random.Range(0, colores.Length);
            cubos[i].GetComponent<Renderer>().material = colores[colornumer];
            numColor[i] = colornumer;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        // Update this position
        Vector3 thisPos = new Vector3(plataforma.transform.position.x, plataforma.transform.position.y, plataforma.transform.position.z);


        // Distance between current position and next position < alloence
        if (Vector3.Distance(thisPos, points[destPoint].position) < allowence)
        {
            UpdateTarget();
        }

        plataforma.transform.position = Vector3.Lerp(plataforma.transform.position, points[destPoint].position, speed  * Time.deltaTime);
    }

    void UpdateTarget()
    {
        if (points.Length == 0)
        {
            return;
        }
        plataforma.transform.position = points[destPoint].position;
        destPoint = (destPoint + 1) % points.Length;

    }
}
