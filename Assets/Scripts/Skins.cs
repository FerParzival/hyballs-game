﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class Skins : MonoBehaviour
{
    [HideInInspector]
    public int texturaElegida;

    public Image[] image;
    public Button[] boton;
    
    public static bool[] isUnlocked = new bool[7];
    private void Start()
    {
        if (GameObject.Find("Skins") != null)
        {
             DontDestroyOnLoad(this.gameObject);
        }
       
    }
    private void Update()
    {
        for (int i = 0; i < isUnlocked.Length; i++)
        {
            if (isUnlocked[i] == true)
            {
                boton[i].interactable = true;
                image[i].gameObject.SetActive(false);
            }
        }
    }
    public void ElegirTextura(int Texture)
    {
        texturaElegida = Texture;
    }
}
